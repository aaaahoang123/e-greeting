import { Injectable } from '@nestjs/common';
import * as nodemailer from 'nodemailer';

@Injectable()
export class MailService {
  private readonly config = {
    service: 'gmail',
    auth: {
      user: 'noreply.egreeting2018@gmail.com',
      pass: 'egreeting@2018',
    },
  };

  private default_option = {
    from: 'sem2shop@gmail.com',
    to: '',
    subject: 'You have receive an e card',
    html: '<h1>Hello</h1>',
  } as any;

  public async sendMail(option: string | {from?, to?, subject?, html?} = null): Promise<any> {
    const transporter = nodemailer.createTransport(this.config);
    let sendOption;
    if (typeof option === 'string') {
      sendOption = {...this.default_option, to: option};
    } else {
      sendOption = {...this.default_option, ...option};
    }

    return await transporter.sendMail(sendOption);
  }
}
