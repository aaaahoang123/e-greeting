import {
  Body,
  Controller,
  Get,
  Inject,
  NotFoundException,
  Param,
  Post,
  Query,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import {
  CategoryModelToken,
  ProductModelToken, ReportModelToken,
  ScheduleModelToken,
  SubscribeModelToken, TransactionModelToken, UserSubscribeModelToken,
} from '../resource/static.resource';
import { Model } from 'mongoose';
import { CategorySchema } from '../database/schema/category.schema';
import * as cloudinary from 'cloudinary';
import { ProductSchema } from '../database/schema/product.schema';
import { SubscribePackSchema } from '../database/schema/subscribe-pack.schema';
import * as moment from 'moment';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Moment } from 'moment';
import { MailService } from '../mail/mail.service';
import { ScheduleSchema } from '../database/schema/schedule.schema';
import { UserSubscribeSchema } from '../database/schema/user-subscribe.schema';
import { PaypalService } from '../paypal/paypal.service';
import { url } from 'inspector';
import { TransactionSchema } from '../database/schema/transaction.schema';
import { ReportSchema } from '../database/schema/report.schema';

@Controller('api')
export class ApiController {
  constructor(
    @Inject(CategoryModelToken) private readonly CategoryModel,
    @Inject(ProductModelToken) private readonly ProductModel,
    @Inject(SubscribeModelToken) private readonly SubscribePackModel: Model<any>,
    @Inject(ScheduleModelToken) private readonly ScheduleModel: Model<any>,
    @Inject(UserSubscribeModelToken) private readonly UserSubscribeModel: Model<any>,
    @Inject(TransactionModelToken) private readonly TransactionModel: Model<any>,
    @Inject(ReportModelToken) private readonly ReportModel: Model<any>,
    private readonly mailService: MailService,
    private readonly paypalService: PaypalService,
  ) {}

  /**
   * Find all the categories or paginate it
   * @param query = {page, limit, sort, search}
   */
  @Get('categories')
  async getCategories(@Query() query) {
    let datas = [];
    if (query.all || Object.keys(query).length === 0) {
      const cate = await this.CategoryModel.find({status: 1}).exec();
      datas = cate.map(c => CategorySchema.getData(c));
      return {
        status: 1,
        message: 'Success',
        datas,
      };
    }

    const page = query.page ? query.page : 1,
      limit = query.limit ? query.limit : 10,
      sort = query.sort && [-1, 1].includes(query.sort) ? {_id: query.sort} : {_id: 1};
    let search = {} as any;

    if (query.search) {
      const template = new RegExp(search);
      search = {name: template, description: template, url: template};
    }

    search = {...search, status: 1};

    const categories = await this.CategoryModel.paginate(search, {page, limit, sort});

    datas = categories.docs;
    delete categories.docs;

    return {
      status: 1,
      message: 'Success',
      datas,
      meta: categories,
    };
  }

  @Get('categories/:url')
  async getCategory(@Param('url') _url) {
    const category = await this.CategoryModel.findOne({url: _url}).exec();
    if (!category) throw new NotFoundException('Not found category');
    return {
      status: 1,
      message: 'Success',
      data: CategorySchema.getData(category),
    };
  }

  /**
   * @param query = {page, limit, sort, search}
   */
  @Get('products')
  async getProducts(@Query() query) {
    if (query.all || Object.keys(query).length === 0) {
      const datas_all = (await this.ProductModel.find().populate('category').exec()).map(p => ProductSchema.getData(p));
      return {
        status: 1,
        message: 'Success',
        datas: datas_all,
      };
    }

    const page = query.page ? Number(query.page) : 1,
      limit = query.limit ? Number(query.limit) : 10,
      sort = query.sort && [-1, 1].includes(query.sort) ? {_id: query.sort} : {_id: 1};
    let search = {status: ProductSchema.ACTIVE_STATUS} as any;

    if (query.search) {
      const template = new RegExp(query.search);
      search = {$or: [{name: template}, {description: template}, {url: template}]};
    }

    if (query.category) {
      const category = await this.CategoryModel.findOne({url: query.category});
      search.category = category._id;
    }

    const products = await this.ProductModel.paginate(search, {page, limit, sort, populate: 'category'});
    let datas = products.docs;
    delete products.docs;
    datas = datas.map(d => ProductSchema.getData(d));
    return {
      status: 1,
      message: 'Success',
      datas,
      meta: products,
    };
  }

  @Get('products/:url')
  async getOneProduct(@Param('url') _url) {
    const product = await this.ProductModel.findOne({url: _url}).populate('category').exec();
    if (!product)
      throw new NotFoundException('Can not found product');

    return {
      status: 1,
      message: 'Success',
      data: ProductSchema.getData(product),
    };
  }

  @Post('transform-image')
  transformImage(@Body() body) {
    const {image_id, transformation} = body;
    return {
      status: 1,
      message: 'Success',
      data: cloudinary.url(image_id, {transformation}),
    };
  }

  @Get('subscribe-packs')
  async getSubscribePacks() {
    const packs = await this.SubscribePackModel.find({status: SubscribePackSchema.ACTIVE_STATUS}).exec();

    return {
      status: 1,
      message: 'Success',
      datas: packs.map(pack => SubscribePackSchema.getData(pack)),
    };
  }

  @Get('subscribe-packs/:id')
  async getOneSubscribePack(@Param('id') id) {
    const pack = await this.SubscribePackModel.findById(id).exec();

    return {
      status: 1,
      message: 'Success',
      data: SubscribePackSchema.getData(pack),
    };
  }

  @UseGuards(JwtAuthGuard)
  @Post('schedules')
  async createSchedule(@Body() {content, send_time, to_emails}, @Req() req, @Res() res) {
    let m_send_time_mls: Moment;
    let status = ScheduleSchema.WAITING_STATUS;
    if (!send_time) {
      m_send_time_mls = moment();
      status = ScheduleSchema.SEND_STATUS;
    } else {
      // Nếu chọn hẹn giờ gửi thì phải subscribe trước mới cho xài.
      const subscribe = await this.UserSubscribeModel.findOne({user_id: req.user.id}).exec();
      if (!subscribe || subscribe.status !== UserSubscribeSchema.ACTIVE_STATUS || subscribe.expire_time < moment().valueOf())
       throw new NotFoundException('You must subscribe our service before using this feature');
      m_send_time_mls = moment(send_time, 'YYYY-MM-DD HH:mm');
    }
    if (!m_send_time_mls.isValid())
      throw new NotFoundException('Invalid send time');

    const send_time_minute = m_send_time_mls.get('m');
    if (send_time_minute < 30 && send_time_minute > 0) {
      m_send_time_mls.set('m', 30);
    } else if (send_time_minute > 0 && send_time_minute < 60) {
      m_send_time_mls.set('m', 0).add(1, 'h');
    }
    const body = {content, send_time: m_send_time_mls.valueOf(), to_emails, user_id: req.user._id, status};
    const schedule = await this.ScheduleModel.create(body);
    res.json({
      status: 1,
      message: 'Success',
    });

    if (!send_time) {
      try {
        await this.mailService.sendMail({
          from: req.user.email,
          to: schedule.to_emails,
          html: schedule.content,
          subject: 'You have receive an e-card from E Greeting',
        });
      } catch (e) {
      }
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('transactions')
  async createTransaction(@Body() body, @Req() req) {
    let protocol = req.secure ? 'https://' : 'http://';
    const host = req.get('host');
    if (host.includes('herokuapp.com'))
      protocol = 'https://';
    const subscribe_pack = await this.SubscribePackModel.findById(body.package);
    if (!subscribe_pack)
      throw new NotFoundException('Not found the package');

    const user = req.user;

    const transaction = await this.TransactionModel.create({
      user_id: user._id,
      package: subscribe_pack._id,
      unit_price: subscribe_pack.price,
    });

    const create_payment_json = {
      intent: 'sale',
      payer: {
        payment_method: 'paypal',
      },
      redirect_urls: {
        return_url: protocol + host + '/api/paypal-accept/' + transaction._id,
        cancel_url: protocol + host + '/api/paypal-cancel/' + transaction._id,
      },
      transactions: [{
        item_list: {
          items: [{
            name: subscribe_pack.name,
            sku: '001',
            price: subscribe_pack.price,
            currency: 'USD',
            quantity: 1,
          }],
        },
        amount: {
          currency: 'USD',
          total: subscribe_pack.price,
        },
        description: `Pay ${subscribe_pack.price} USD for E-greeting ${subscribe_pack.name} pack`,
      }],
    };

    const payment = await this.paypalService.create(create_payment_json);
    for (const link of payment.links) {
      if (link.rel === 'approval_url') {
        return {
          status: 1,
          message: 'Success',
          data: link.href,
        };
      }
    }
  }

  @Get('paypal-accept/:id')
  async acceptPayment(@Query() {paymentId, PayerID}, @Param('id') id, @Res() res) {
    const transaction = await this.TransactionModel.findById(id).populate('package').exec();
    if (!transaction)
      throw new NotFoundException('Not found the transaction');

    const execute_payment_json = {
      payer_id: PayerID,
      transactions: [
        {
          amount: {
            currency: 'USD',
            total: transaction.unit_price,
          },
        },
      ],
    };
    const payment = this.paypalService.execute(paymentId, execute_payment_json);
    const userSubscribePromise = this.UserSubscribeModel.findOne({user_id: transaction.user_id}).exec();

    // thực thi thanh toán
    await payment;
    let userSubscribe = await userSubscribePromise;
    if (!userSubscribe) {
      userSubscribe = this.UserSubscribeModel.create({
        user_id: transaction.user_id,
        expire_time: moment().valueOf() + transaction.package.effective_long,
      });
    } else {
      userSubscribe.expire_time += transaction.package.effective_long;
      userSubscribe = userSubscribe.save();
    }

    transaction.status = TransactionSchema.PAID_STATUS;
    await transaction.save();
    await userSubscribe;

    res.redirect('/#!/?message=Payment success');
  }

  @Get('paypal-cancel/:id')
  async cancelPayment(@Param() id, @Res() res) {
    await this.TransactionModel.findByIdAndUpdate(id, {status: TransactionSchema.REJECT_STATUS}).exec();

    res.redirect('/#!/?message=Payment canceled');
  }

  @Post('reports')
  @UseGuards(JwtAuthGuard)
  async createReport(@Body() {message}, @Req() req) {
    const report = await this.ReportModel.create({
      user_id: req.user._id,
      message,
    });

    return {
      status: 1,
      message: 'Success',
      data: ReportSchema.getData(report),
    };
  }

  @Get('scan-schedules')
  async scanSchedules(@Res() res) {
    res.json('Success');
    const now = moment().valueOf();

    const schedules = await this.ScheduleModel.find({
      send_time: {
        $lt: now,
      },
      status: ScheduleSchema.WAITING_STATUS,
    }).populate('user_id').exec();

    for (const schedule of schedules) {
      try {
        await this.ScheduleModel.findByIdAndUpdate(schedule._id, {status: ScheduleSchema.SEND_STATUS}).exec();
        await this.mailService.sendMail({
          from: schedule.user_id.email ? schedule.user_id.email : '',
          to: schedule.to_emails,
          html: schedule.content,
          subject: 'You have receive an e-card from E Greeting',
        });
      } catch (e) {
        console.log(e.message);
      }
    }
  }
}