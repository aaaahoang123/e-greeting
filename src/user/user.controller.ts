import { Body, Controller, Inject, Post } from '@nestjs/common';
import { Model } from 'mongoose';
import { UserSchema } from '../database/schema/user.schema';
import { UserModelToken } from '../resource/static.resource';

@Controller('user')
export class UserController {
  constructor(@Inject(UserModelToken) private readonly UserModel: Model<any>) {}

  @Post()
  async Register(@Body() user: any) {
    const newUser = new this.UserModel(user);
    await newUser.save();
    return UserSchema.getData(newUser);
  }

  @Post('login')
  async Login(@Body() user) {

  }
}
