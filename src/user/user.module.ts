import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/database/database.module';
import { UserController } from './user.controller';
import { UserSchema } from '../database/schema/user.schema';

@Module({
  imports: [DatabaseModule],
  controllers: [UserController],
  providers: [...UserSchema.getInstance().provider],
})
export class UserModule {}
