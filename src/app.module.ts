import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { ApiModule } from './api/api.module';
import { AdminModule } from './admin/admin.module';
import { MailService } from './mail/mail.service';
import { PaypalService } from './paypal/paypal.service';
@Module({
  imports: [DatabaseModule, UserModule, AuthModule, ApiModule, AdminModule],
  controllers: [AppController],
  providers: [AppService, MailService, PaypalService],
})
export class AppModule {}
