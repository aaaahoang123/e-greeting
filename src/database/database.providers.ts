import * as mongoose from 'mongoose';
import { DbConnectionToken } from '../resource/static.resource';

const dbConnection = 'mongodb://admin123:admin123@ds139198.mlab.com:39198/e_greeting_db';
// const dbConnection = 'mongodb://localhost/e_greeting_db';
export const databaseProviders = [
  {
    provide: DbConnectionToken,
    useFactory: async (): Promise<typeof mongoose> => {
      (mongoose as any).Promise = global.Promise;
      return await mongoose.connect(dbConnection, { useNewUrlParser: true });
    },
  },
];
