import { Connection, Schema } from 'mongoose';
import { DbConnectionToken, ReportModelToken } from '../../resource/static.resource';
import * as moment from 'moment';
import { billingAgreement } from 'paypal-rest-sdk';
import update = billingAgreement.update;

export class ReportSchema {
  public static instance: ReportSchema;
  public provider: any;
  readonly schema: Schema;

  public static readonly READ_STATUS = 1;
  public static readonly UNREAD_STATUS = -1;

  public static readonly status_title = {
    '1': 'Read',
    '-1': 'Unread',
  };

  public static getInstance(): ReportSchema {
    if (!this.instance)
      this.instance = new ReportSchema();
    return this.instance;
  }

  constructor() {
    this.schema = new Schema({
      user_id: {
        type: Schema.Types.ObjectId,
        required: true,
      },
      message: {
        type: String,
        required: [true, 'Please input some message'],
      },
      created_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      updated_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      status: {
        type: Number,
        required: true,
        default: ReportSchema.UNREAD_STATUS,
      },
    });

    this.provider = [
      {
        provide: ReportModelToken,
        useFactory: (connection: Connection) => connection.model('reports', this.schema),
        inject: [DbConnectionToken],
      },
    ];
  }

  public static getData({_id, user_id, message, updated_at, created_at, status}) {
    return {
      _id,
      user_id,
      message,
      created_at: moment(created_at).format('HH:mm DD/MM/YYYY'),
      updated_at: moment(updated_at).format('HH:mm DD/MM/YYYY'),
      status,
      status_title: ReportSchema.status_title[status],
    };
  }
}