import { Connection, Schema } from 'mongoose';
import { DbConnectionToken, ProductModelToken } from '../../resource/static.resource';
import * as paginate from 'mongoose-paginate-v2';
import * as moment from 'moment';
import { CategorySchema } from './category.schema';

export class ProductSchema {
  public static instance: ProductSchema;
  public provider: any;
  readonly schema: Schema;

  public static readonly ACTIVE_STATUS = 1;
  public static readonly UNACTIVE_STATUS = -1;

  public static readonly status_title = {
    '1': 'Active',
    '-1': 'Inactive',
  };

  public static getInstance(): ProductSchema {
    if (!this.instance) this.instance = new ProductSchema();
    return this.instance;
  }

  constructor() {
    this.schema = new Schema({
      name: {
        type: String,
        required: [true, 'Name is required'],
      },
      url: {
        type: String,
        required: [true, 'Url is required'],
      },
      description: {
        type: String,
        required: [true, 'Description is required'],
      },
      image: {
        type: String,
        required: [true, 'Image is required'],
      },
      video: {
        type: String,
        required: [true, 'Video is required'],
      },
      category: {
        type: Schema.Types.ObjectId,
        required: [true, 'Please input category'],
        ref: 'categories',
      },
      created_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      updated_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      status: {
        type: Number,
        required: true,
        default: ProductSchema.ACTIVE_STATUS,
      },
    });

    this.schema.plugin(paginate);

    this.provider = [
      {
        provide: ProductModelToken,
        useFactory: (connection: Connection) => connection.model('products', this.schema),
        inject: [DbConnectionToken],
      },
    ];
  }

  public static getData({_id, name, url, description, image, video, created_at, updated_at, status, category}) {
    const data = {
      _id,
      name,
      url,
      description,
      image,
      video,
      created_at: moment(created_at).format('YYYY-MM-DD HH:mm:ss'),
      updated_at: moment(updated_at).format('YYYY-MM-DD HH:mm:ss'),
      status,
      status_title: ProductSchema.status_title[status],
    } as any;

    if (category) {
      data.category = CategorySchema.getData(category);
      data.category_id = category._id;
    }
    return data;
  }
}