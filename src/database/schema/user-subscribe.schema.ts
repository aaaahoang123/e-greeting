import { Connection, Schema } from 'mongoose';
import { DbConnectionToken, SubscribeModelToken, UserSubscribeModelToken } from '../../resource/static.resource';
import * as moment from 'moment';

export class UserSubscribeSchema {
  public static instance: UserSubscribeSchema;
  public provider: any;
  public schema: Schema;

  public static readonly ACTIVE_STATUS = 1;
  public static readonly INACTIVE_STATUS = -1;

  public static readonly status_title = {
    '1': 'Active',
    '-1': 'Inactive',
  };

  public static getInstance() {
    if (!this.instance)
      this.instance = new UserSubscribeSchema();

    return this.instance;
  }

  constructor() {
    this.schema = new Schema({
      user_id: {
        type: Schema.Types.ObjectId,
        ref: 'users',
      },
      expire_time: {
        type: Number,
        required: true,
      },
      created_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      updated_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      status: {
        type: Number,
        required: true,
        default: UserSubscribeSchema.ACTIVE_STATUS,
        validate: {
          validator: (value) => [UserSubscribeSchema.ACTIVE_STATUS, UserSubscribeSchema.INACTIVE_STATUS].includes(value),
          message: `Status must be ${UserSubscribeSchema.ACTIVE_STATUS} or ${UserSubscribeSchema.INACTIVE_STATUS}`,
        },
      },
    });

    this.provider = [
      {
        provide: UserSubscribeModelToken,
        useFactory: (connection: Connection) => connection.model('user_subscribes', this.schema),
        inject: [DbConnectionToken],
      },
    ];
  }

  public getData(userSubscribe) {
    return {
      _id: userSubscribe._id,
      expire_time: moment(userSubscribe.expire_time).format('YYYY-MM-DD HH:mm:ss'),
      created_at: moment(userSubscribe.created_at).format('YYYY-MM-DD HH:mm:ss'),
      updated_at: moment(userSubscribe.updated_at).format('YYYY-MM-DD HH:mm:ss'),
      status: userSubscribe.status,
      status_title: userSubscribe.status_title[userSubscribe.status],
    };
  }
}