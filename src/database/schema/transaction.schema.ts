import { Connection, Schema } from 'mongoose';
import { DbConnectionToken, TransactionModelToken } from '../../resource/static.resource';
import { UserSchema } from './user.schema';
import { SubscribePackSchema } from './subscribe-pack.schema';
import * as moment from 'moment';

export class TransactionSchema {
  public static instance: TransactionSchema;
  public provider: any;
  readonly schema: Schema;

  public static readonly PAID_STATUS = 1;
  public static readonly REJECT_STATUS = -1;
  public static readonly UNPAID_STATUS = 0;

  public static readonly status_title = {
    '1': 'Paid',
    '-1': 'Reject',
    '0': 'Unpaid',
  };

  public static getInstance(): TransactionSchema {
    if (!this.instance)
      this.instance = new TransactionSchema();
    return this.instance;
  }

  constructor() {
    this.schema = new Schema({
      user_id: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'users',
      },
      package: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'subscribe_packs',
      },
      unit_price: {
        type: Number,
        required: true,
      },
      created_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      updated_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      status: {
        type: Number,
        required: true,
        default: TransactionSchema.UNPAID_STATUS,
        validate: {
          validator: (val) => [TransactionSchema.PAID_STATUS, TransactionSchema.UNPAID_STATUS, TransactionSchema.REJECT_STATUS].includes(val),
        },
      },
    });

    this.provider = [
      {
        provide: TransactionModelToken,
        useFactory: (connection: Connection) => connection.model('transactions', this.schema),
        inject: [DbConnectionToken],
      },
    ];
  }

  public static getData(transaction) {
    return {
      _id: transaction._id,
      user_id: transaction.user_id._id,
      user: UserSchema.getData(transaction.user_id),
      package: SubscribePackSchema.getData(transaction.package),
      package_id: transaction.package._id,
      unit_price: transaction.unit_price,
      created_at: moment(transaction.created_at).format('MM/DD/YYYY'),
      updated_at: moment(transaction.updated_at).format('MM/DD/YYYY'),
      status: transaction.status,
      status_title: TransactionSchema.status_title[transaction.status],
    };
  }
}