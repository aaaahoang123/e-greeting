import { Connection, Model, Schema } from 'mongoose';
import * as bcrypt from 'bcryptjs';
import { DbConnectionToken, UserModelToken } from '../../resource/static.resource';
import * as moment from 'moment';

export class UserSchema {
  readonly schema: Schema;
  public provider: any;
  private static instance: UserSchema;

  public static getInstance(): UserSchema {
    if (!this.instance) this.instance = new UserSchema();
    return this.instance;
  }

  public static ACTIVE_STATUS = 1;
  public static INACTIVE_STATUS = -1;

  public static status_title() {
    return {
      '1': 'Active',
      '-1': 'Inactive',
    };
  }
  constructor() {
    this.schema = new Schema({
      username: {
        type: String,
        unique: true,
        required: [true, 'Username is required!'],
        minlength: [8, 'Username must have at least 8 characters!'],
        maxlength: [50, 'Username cannot longer than 50 characters!'],
        validate: {
          validator: (v: string) => {
            return /^[a-zA-Z0-9]+$/.test(v);
          },
          message: 'Username only have characters and numbers!',
        },
      },
      password: {
        type: String,
        required: [true, 'Password is required!'],
        minlength: [8, 'Password must have at least 8 characters!'],
        validate: {
          validator: (pass: string) => {
            return !pass.includes(' ');
          },
          message: 'Password must not contain space!',
        },
      },
      full_name: {
        type: String,
      },
      address: {
        type: String,
      },
      email: {
        type: String,
        required: [true, 'Email is required!'],
        minlength: [8, 'Email must have at least 8 character!'],
        validate: {
          validator: (mail: string) => {
            return /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(mail);
          },
          message: (props: any) => `${props.value} is not a valid email!`,
        },
      },
      phone: {
        type: String,
        default: null,
        validate: {
          validator: (pn: string) => {
            /**
             * Match for:
             * - head: +84 or +(84), from 2 to 4 numbers
             * - second: at least 3 numbers.
             * - third: at least 3 numbers.
             * - last: at least 1 numbers.
             * - total: at least 7 numbers, not include head.
             * - each part can split by -, space, ., or nothing
             */
            return !pn || /^[\+]?[(]?[0-9]{2,4}[)]?[-\s\.*]?[0-9]{3,}?[-\s\.*]?[0-9]{3,}?[-\s\.*]?[0-9]{1,}$/im.test(pn);
          },
          message: (props: any) => `${props.value} is not a valid phone number!`,
        },
      },
      gender: {
        type: Number,
        default: 0,
      },
      policy: {
        type: Schema.Types.ObjectId,
        default: null,
        ref: 'policies',
      },
      birthday: {
        type: Number,
        default: 0,
      },
      created_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      updated_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      status: {
        type: Number,
        required: true,
        default: 1,
      },
    });

    this.schema.pre('save',  function(next) {
      const user = this;
      // only hash the password if it has been modified (or is new)
      if (!this.isModified('password')) return next();

      // generate a salt
      bcrypt.genSalt(Math.round(Math.random() * 10), (e: any, salt: any) => {
        if (e) return next(e);

        // hash the password using our new salt
        // @ts-ignore
        bcrypt.hash(user.password, salt, (err: any, hash: any) => {
          if (err) return next(err);

          // override the cleartext password with the hashed one
          // @ts-ignore
          user.password = hash;
          next();
        });
      });
    });

    this.provider = [
      {
        provide: UserModelToken,
        useFactory: (connection: Connection) => connection.model('users', this.schema),
        inject: [DbConnectionToken],
      },
    ];
  }

  public static getData({_id, username, email, phone, gender, birthday, created_at, updated_at, status, policy}) {
    if (!username) return null;
    return {
      _id,
      username,
      email,
      phone,
      gender: gender ? gender : 1,
      birthday: moment(birthday).format('YYYY/MM/DD'),
      birthday_format: moment(birthday).format('YYYY/MM/DD'),
      policy: policy ? policy : 0,
      created_at: new Date(created_at),
      updated_at: new Date(updated_at),
      status,
      status_title: this.status_title()[status],
    };
  }
}