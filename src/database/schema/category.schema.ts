import { CategoryModelToken, DbConnectionToken, UserModelToken } from '../../resource/static.resource';
import { Connection, Schema } from 'mongoose';
import * as paginate from 'mongoose-paginate-v2';
import * as moment from 'moment';

export class CategorySchema {
  public static instance: CategorySchema;
  public provider;
  readonly schema: Schema;

  public static readonly ACTIVE_STATUS = 1;
  public static readonly INACTIVE_STATUS = -1;
  public static readonly status_title = {
    '1': 'Active',
    '-1': 'Inactive',
  };
  
  public static getInstance(): CategorySchema {
    if (!this.instance) this.instance = new CategorySchema();
    return this.instance;
  }
  
  constructor() {
    this.schema = new Schema({
      name: {
        type: String,
        required: [true, 'Name is required!'],
      },
      url: {
        type: String,
        unique: true,
        required: [true, 'Url is required'],
        validate: {
          validator: (v: string) => /^[\w-_]*$/.test(v),
          message: `Url just have characters, numbers, and '-' or '_'!`,
        },
      },
      description: {
        type: String,
        required: [true, 'Description is required!'],
      },
      level: {
        type: Number,
        required: [true, 'Category must have a level! Level 1 will be display as primary category! Level 2 will be dropdown in other.'],
        validate: {
          validator: (lvl) => [1, 2].includes(lvl),
          message: 'Category level must be 1 or 2',
        },
      },
      created_at: {
        type: Number,
        default: Date.now,
        required: true,
      },
      updated_at: {
        type: Number,
        default: Date.now,
        required: true,
      },
      status: {
        type: Number,
        required: true,
        default: CategorySchema.ACTIVE_STATUS,
      },
    });

    this.schema.plugin(paginate);

    this.provider = [
      {
        provide: CategoryModelToken,
        useFactory: (connection: Connection) => connection.model('categories', this.schema),
        inject: [DbConnectionToken],
      },
    ];
  }

  public static getData({_id, name, url, description, level, created_at, updated_at, status}) {
    if (!url) return null;
    return {
      _id,
      name,
      url,
      description,
      level,
      created_at: moment(created_at).format('YYYY-MM-DD HH:mm:ss'),
      updated_at: moment(updated_at).format('YYYY-MM-DD HH:mm:ss'),
      status,
      status_title: CategorySchema.status_title[status],
    };
  }
}