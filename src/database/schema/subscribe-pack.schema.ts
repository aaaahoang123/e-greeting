import { Connection, Schema } from 'mongoose';
import { DbConnectionToken, SubscribeModelToken } from '../../resource/static.resource';
import * as moment from 'moment';

export class SubscribePackSchema {
  public static instance: SubscribePackSchema;
  public provider: any;
  readonly schema: Schema;

  public static readonly ACTIVE_STATUS = 1;
  public static readonly INACTIVE_STATUS = -1;

  public static readonly status_title = {
    '1': 'Active',
    '-1': 'Inactive',
  };

  public static getInstance(): SubscribePackSchema {
    if (!this.instance)
      this.instance = new SubscribePackSchema();
    return this.instance;
  }

  constructor() {
    this.schema = new Schema({
      name: {
        type: String,
        required: [true, 'Name of subscribe is required'],
      },
      effective_long: {
        type: Number,
        required: [true, 'Please input how long this subscribe will effective'],
      },
      price: {
        type: Number,
        required: [true, 'Please enter the price'],
      },
      created_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      updated_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      status: {
        type: Number,
        required: true,
        default: SubscribePackSchema.ACTIVE_STATUS,
        validate: {
          validator: (value) => [SubscribePackSchema.ACTIVE_STATUS, SubscribePackSchema.INACTIVE_STATUS].includes(value),
          message: `Status must be ${SubscribePackSchema.ACTIVE_STATUS} or ${SubscribePackSchema.INACTIVE_STATUS}`,
        },
      },
    });

    this.provider = [
      {
        provide: SubscribeModelToken,
        useFactory: (connection: Connection) => connection.model('subscribe_packs', this.schema),
        inject: [DbConnectionToken],
      },
    ];
  }

  public static getData(subscribePack) {
    if (!subscribePack || !subscribePack.effective_long)
      return null;
    return {
      _id: subscribePack._id,
      name: subscribePack.name,
      effective_long: subscribePack.effective_long / (1000 * 60 * 60 * 24),
      price: subscribePack.price,
      created_at: moment(subscribePack.created_at).format('YYYY-MM-DD HH:mm:ss'),
      updated_at: moment(subscribePack.updated_at).format('YYYY-MM-DD HH:mm:ss'),
      status: subscribePack.status,
      status_title: SubscribePackSchema.status_title[subscribePack.status],
    };
  }
}