import { Connection, Schema } from 'mongoose';
import { DbConnectionToken, ScheduleModelToken } from '../../resource/static.resource';
import * as moment from 'moment';
import { UserSchema } from './user.schema';

export class ScheduleSchema {
  private static instance: ScheduleSchema;
  public provider: any;
  readonly schema: Schema;

  public static readonly SEND_STATUS = 1;
  public static readonly WAITING_STATUS = 0;

  public static readonly status_title = {
    1: 'Sent',
    0: 'Waiting',
  };

  public static getInstance(): ScheduleSchema {
    if (!this.instance)
      this.instance = new ScheduleSchema();
    return this.instance;
  }

  constructor() {
    this.schema = new Schema({
      user_id: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'users',
      },
      content: {
        type: String,
        required: [true, 'Please input the content'],
      },
      send_time: {
        type: Number,
        required: true,
        validate: {
          validator: (value) => value >= moment().valueOf(),
          message: 'The send time must now or future',
        },
      },
      to_emails: {
        type: [String],
        required: [true, 'Please choose email to send'],
        validate: {
          validator: (val) => {
            return val[0];
          },
          message: 'Please input at least one mail to send',
        },
      },
      created_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      updated_at: {
        type: Number,
        required: true,
        default: Date.now,
      },
      status: {
        type: Number,
        required: true,
        default: ScheduleSchema.WAITING_STATUS,
        validate: {
          validator: (val) => [ScheduleSchema.WAITING_STATUS, ScheduleSchema.SEND_STATUS].includes(val),
        },
      },
    });

    this.provider = [
      {
        provide: ScheduleModelToken,
        useFactory: (connection: Connection) => connection.model('schedules', this.schema),
        inject: [DbConnectionToken],
      },
    ];
  }

  public static getData(schedule) {
    return {
      _id: schedule._id,
      user_id: schedule.user_id._id,
      user: UserSchema.getData(schedule.user_id),
      content: schedule.content,
      send_time: moment(schedule.send_time).format('YYYY-MM-DD HH:mm:ss'),
      to_emails: schedule.to_emails,
      created_at: moment(schedule.created_at).format('YYYY-MM-DD HH:mm:ss'),
      updated_at: moment(schedule.updated_at).format('YYYY-MM-DD HH:mm:ss'),
      status: schedule.status,
      status_title: ScheduleSchema.status_title[schedule.status],
    };
  }
}