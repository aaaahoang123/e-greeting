import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './http-exception.filter';
import * as fileUpload from 'express-fileupload';
import { join } from 'path';
import { CloudinaryConfig, PayPalConfig } from './resource/static.resource';
import * as cloudinary from 'cloudinary';
import * as cors from 'cors';
import * as paypal from 'paypal-rest-sdk';
import * as cron from 'node-cron';
import Axios from 'axios';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const port = process.env.PORT || 3000;
  cloudinary.config(CloudinaryConfig);
  paypal.configure(PayPalConfig);

  cron.schedule('*/5 * * * *', () => {
    const url = 'https://e-greeting.herokuapp.com/api/scan-schedules';
    // const url = 'http://localhost:3000/api/scan-schedules';
    Axios.get(url)
      .then(res => {
        console.log(res.data);
      })
      .catch(e => {
        console.log(e.message);
      });
  });
  app
    .use(fileUpload())
    .useStaticAssets(join(__dirname, '../public'))
    .use(cors())
    .useGlobalFilters(new HttpExceptionFilter());
  await app.listen(port);
}
bootstrap();
