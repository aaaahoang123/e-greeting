import { Injectable } from '@nestjs/common';
import * as paypal from 'paypal-rest-sdk';
import { rejects } from 'assert';
@Injectable()
export class PaypalService {
  public create(option): Promise<any> {
    return new Promise((resolve, reject) => {
      paypal.payment.create(option, (err, result) => {
        if (err)
          reject(err);
        else
          resolve(result);
      });
    });
  }

  public execute(paymentId, option): Promise<any> {
    return new Promise((resolve, reject) => {
      paypal.payment.execute(paymentId, option, (err, result) => {
        if (err)
          reject(err);
        else
          resolve(result);
      });
    });
  }
}
