import { Body, Controller, Get, Inject, NotFoundException, Param, Post, Put, Query, Req, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../auth/roles.decorator';
import {
  CategoryModelToken,
  ProductModelToken, ReportModelToken,
  ScheduleModelToken,
  SubscribeModelToken,
  TransactionModelToken,
  UserSubscribeModelToken,
} from '../resource/static.resource';
import { Model } from 'mongoose';
import { uploadCloudinaryByStream } from '../resource/helper';
import { CategorySchema } from '../database/schema/category.schema';
import * as moment from 'moment';
import { ProductSchema } from '../database/schema/product.schema';
import { SubscribePackSchema } from '../database/schema/subscribe-pack.schema';
import { ScheduleSchema } from '../database/schema/schedule.schema';
import { asap } from 'rxjs/internal/scheduler/asap';
import { TransactionSchema } from '../database/schema/transaction.schema';

@Controller('admin')
@UseGuards(JwtAuthGuard)
@Roles('admin')
export class AdminController {
  constructor(
    @Inject(CategoryModelToken) private readonly CategoryModel: Model<any>,
    @Inject(ProductModelToken) private readonly ProductModel: Model<any>,
    @Inject(SubscribeModelToken) private readonly SubscribePackModel: Model<any>,
    @Inject(UserSubscribeModelToken) private readonly UserSubscribeModel: Model<any>,
    @Inject(ScheduleModelToken) private readonly ScheduleModel: Model<any>,
    @Inject(TransactionModelToken) private readonly TransactionModel: Model<any>,
    @Inject(ReportModelToken) private readonly ReportModel: Model<any>,
  ) {
  }

  @Post('categories')
  async postCategories(@Body() body) {
    const category = await this.CategoryModel.create(body);
    return {
      status: 1,
      message: 'Success',
      data: CategorySchema.getData(category),
    };
  }

  @Put('categories/:url')
  async editCategories(@Body() { name, description, level, status }, @Param('url') url) {
    const new_data = { name, description, level, status, updated_at: moment() };
    const category = await this.CategoryModel.findOneAndUpdate({ url }, new_data, { new: true }).exec();
    if (!category) throw new NotFoundException('Not found category');
    return {
      status: 1,
      message: 'Success',
      data: CategorySchema.getData(category),
    };
  }

  @Get('categories/:url/delete')
  async deleteCategory(@Param('url') url) {
    const category = await this.CategoryModel.findOneAndUpdate({ url }, { status: -1 }, { new: true }).exec();
    if (!category) throw new NotFoundException('Not found category');
    return {
      status: 1,
      message: 'Success',
      data: CategorySchema.getData(category),
    };
  }

  @Post('products')
  async postProduct(@Body() body) {
    return {
      status: 1,
      message: 'Success',
      data: await this.ProductModel.create(body),
    };
  }

  @Put('products/:url')
  async editProduct(@Body() { name, description, video, image, status, category_id }, @Param('url') url) {
    const newData = { name, description, video, image, status, category: category_id, updated_at: moment() };
    const product = await this.ProductModel.findOneAndUpdate({ url }, newData, { new: true }).exec();
    if (!product) throw new NotFoundException('Not found product');
    return {
      status: 1,
      message: 'Success',
      data: ProductSchema.getData(product),
    };
  }

  @Get('products/:url/delete')
  async deleteProduct(@Param('url') url) {
    const product = await this.ProductModel.findOneAndUpdate({ url }, { status: ProductSchema.UNACTIVE_STATUS }, { new: true }).exec();
    if (!product) throw new NotFoundException('Not found product');
    return {
      status: 1,
      message: 'Success',
      data: ProductSchema.getData(product),
    };
  }

  @Post('upload-image')
  async uploadImage(@Req() req) {
    if (!req.files.image)
      throw new NotFoundException('Must upload a file in field image');
    const result = await uploadCloudinaryByStream(req.files.image.data);
    return {
      status: 1,
      message: 'Success',
      data: {
        image_id: result.public_id,
        image_url: result.secure_url,
      },
    };
  }

  @Post('upload-video')
  async uploadVideo(@Req() req) {
    if (!req.files.video)
      throw new NotFoundException('Must upload a file in field video');

    const result = await uploadCloudinaryByStream(req.files.video.data, 'video');
    return {
      status: 1,
      message: 'Success',
      data: {
        video_id: result.public_id,
        video_url: result.secure_url,
      },
    };
  }

  @Post('subscribe-packs')
  async createSubscribePack(@Body() { name, effective_long, price }) {
    const body = {
      name,
      price,
      effective_long: effective_long ? effective_long * 24 * 60 * 60 * 1000 : null,
    };

    await this.SubscribePackModel.create(body);

    return {
      status: 1,
      message: 'Success',
    };
  }

  @Put('subscribe-packs/:id')
  async editSubscribePack(@Body() { name, effective_long, price, status }, @Param('id') id) {
    const body = { name, price, updated_at: moment().valueOf(), status } as any;
    if (effective_long)
      body.effective_long = effective_long * 24 * 60 * 60 * 1000;

    await this.SubscribePackModel.findByIdAndUpdate(id, body).exec();

    return {
      status: 1,
      message: 'Success',
    };
  }

  @Post('subscribe-packs/:id/delete')
  async deleteSubscribePack(@Param('id') id) {
    await this.SubscribePackModel.findByIdAndUpdate(id, { status: SubscribePackSchema.INACTIVE_STATUS }).exec();
    return {
      status: 1,
      message: 'Success',
    };
  }

  @Get('schedules')
  async getSchedules(@Query() query) {
    const schedules = await this.ScheduleModel.find().populate('user_id').exec();
    const datas = schedules.map(schedule => ScheduleSchema.getData(schedule));

    return {
      status: 1,
      message: 'Success',
      datas,
    };
  }

  @Get('transactions')
  async getTransactions(@Query() { time_from, time_to, search, status }) {
    const query = {} as any;

    if (time_from) {
      query.created_at = { $gt: moment(time_from, 'MM/DD/YYYY').valueOf() } as any;
    }

    if (time_to) {
      if (!query.created_at)
        query.created_at = {} as any;

      query.created_at.$lt = moment(time_to, 'MM/DD/YYYY').valueOf();
    }

    if (status)
      query.status = status;

    const transactions = await this.TransactionModel.find(query).sort({ created_at: -1 }).populate('package').populate('user_id').exec();

    const datas = transactions.map(transaction => TransactionSchema.getData(transaction));

    return {
      status: 1,
      message: 'Success',
      datas,
    };
  }

  @Put('transactions')
  async editTransaction(@Query() { id, type }) {
    let data;
    const transactionPromise = this.TransactionModel.findById(id).populate('package').exec();
    switch (type) {
      case 'paid':
        data = { status: TransactionSchema.PAID_STATUS };
        break;
      case 'reject':
        data = { status: TransactionSchema.REJECT_STATUS };
        break;
      default:
        data = { status: TransactionSchema.UNPAID_STATUS };
        break;
    }
    const transaction = await transactionPromise;
    const updatePromise = transaction.update(data).exec();
    if (transaction.package) {
      if (transaction.status === TransactionSchema.PAID_STATUS && type !== 'paid') {
        await this.UserSubscribeModel
          .findOneAndUpdate({ user_id: transaction.user_id }, { $inc: { expire_time: -transaction.package.effective_long } })
          .exec();
      }
      if (transaction.status !== TransactionSchema.PAID_STATUS && type === 'paid') {
        await this.UserSubscribeModel
          .findOneAndUpdate({ user_id: transaction.user_id }, { $inc: { expire_time: transaction.package.effective_long } })
          .exec();
      }
    }
    await updatePromise;
    return {
      status: 1,
      message: 'Success',
    };
  }

  @Get('dashboard-data')
  async getDashboardData(@Req() req, @Query() { date }) {
    let m_date = moment().second(0).minute(0).millisecond(0).hour(0);
    if (date) {
      m_date = moment(date, 'YYYY-MM-DD');
    }
    const baseQuery = {
        created_at: {
          $gt: m_date.valueOf(),
          $lt: m_date.add(1, 'd').valueOf(),
        },
    };

    const transactionsPromise = this.TransactionModel.aggregate([
      {
        $match: baseQuery,
      },
      {
        $facet: {
          quantity_revenue: [
            {
              $group: {
                _id: '$status',
                qty: {
                  $sum: 1,
                },
                revenue: {
                  $sum: '$unit_price',
                },
              },
            },
          ],
        },
      },
    ]).exec();

    const schedulePromise = this.ScheduleModel.aggregate([
      {
        $match: baseQuery,
      },
      {
        $facet: {
          quantity: [
            {
              $group: {
                _id: '$status',
                qty: {
                  $sum: 1,
                },
              },
            },
          ],
        },
      },
    ]).exec();

    return {
      status: 1,
      message: 'Success',
      data: {
        transactions: await transactionsPromise,
        schedule: await schedulePromise,
      },
    };
  }
}
