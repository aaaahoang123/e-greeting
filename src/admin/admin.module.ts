import { AdminController } from './admin.controller';
import { JwtSecret } from '../resource/static.resource';
import { UserSchema } from '../database/schema/user.schema';
import { AuthService } from '../auth/auth.service';
import { JwtStrategy } from '../auth/jwt.strategy';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { CategorySchema } from '../database/schema/category.schema';
import { ProductSchema } from '../database/schema/product.schema';
import { SubscribePackSchema } from '../database/schema/subscribe-pack.schema';
import { UserSubscribeSchema } from '../database/schema/user-subscribe.schema';
import { TransactionSchema } from '../database/schema/transaction.schema';
import { ScheduleSchema } from '../database/schema/schedule.schema';
import { ReportSchema } from '../database/schema/report.schema';

@Module({
  controllers: [AdminController],
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secretOrPrivateKey: JwtSecret,
    }),
  ],
  providers: [
    ...UserSchema.getInstance().provider,
    ...CategorySchema.getInstance().provider,
    ...ProductSchema.getInstance().provider,
    ...SubscribePackSchema.getInstance().provider,
    ...UserSubscribeSchema.getInstance().provider,
    ...TransactionSchema.getInstance().provider,
    ...ScheduleSchema.getInstance().provider,
    ...ReportSchema.getInstance().provider,
    AuthService,
    JwtStrategy,
  ],
})
export class AdminModule {}
