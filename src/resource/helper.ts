import { NotFoundException } from '@nestjs/common';
import * as cloudinary from 'cloudinary';

export const uploadCloudinaryByStream = (buffer: Buffer, resource_type: string = 'image'): Promise<any> => {
    return new Promise((resolve, reject) => {
        if (!buffer)
          throw new NotFoundException('Not found image');

        const callback = (error, result) => {
          if (error)
            reject(error);
          else
            resolve(result);
        };

        cloudinary
          .v2
          .uploader
          .upload_stream({ resource_type }, callback)
          .end(buffer);
    });
};