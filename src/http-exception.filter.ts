import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from '@nestjs/common';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const res = host.switchToHttp().getResponse();
    let status;
    try {
      if (exception instanceof HttpException) {
        status = exception.getStatus();
        return res.status(status).json({
          status,
          message: exception.message.message,
        });
      }

      if (exception.name) {
        if (exception.name === 'ValidationError') {
          status = 404;
          return res.status(status).json({
            status,
            errors: exception.errors,
            message: exception._message,
          });
        }
        if (exception.name === 'MongoError') {
          if (exception.code === 11000) {
            return res.status(409).json({status: 409, errors: {conflict: {name: 'Conflict Error', message: exception.message}}});
          }
        }
      }

      status = HttpStatus.INTERNAL_SERVER_ERROR;
      return res.status(status).json({
        status,
        message: exception.message,
      });
    } catch (e) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          message: e.message,
        });
    }
  }
}
