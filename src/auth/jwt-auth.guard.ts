import { ExecutionContext, Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AuthGuard } from '@nestjs/passport';
import { Model } from 'mongoose';
import { Reflector } from '@nestjs/core';
import { UserModelToken } from '../resource/static.resource';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  roles;
  constructor(@Inject(UserModelToken) private readonly UserModel: Model<any>, private readonly reflector: Reflector){
    super();
  }

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    this.roles = this.reflector.get<string[]>('roles', context.getHandler());
    return super.canActivate(context);
  }

  handleRequest(err, user, info) {
    // this.roles = ['string'] => lấy string này để tìm policy của user, xem có policy mang name này không. Nếu có thì đi tiếp, không thì bắn lỗi
    if (err || !user) {
      throw err || new UnauthorizedException();
    }
    return user;
  }
}
