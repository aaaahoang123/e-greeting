import { Body, Controller, Get, Inject, Post, Put, Req, UnauthorizedException, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './jwt-auth.guard';
import { Roles } from './roles.decorator';
import { Model } from 'mongoose';
import { UserSchema } from '../database/schema/user.schema';
import * as bcrypt from 'bcryptjs';
import { UserModelToken } from '../resource/static.resource';
import * as moment from 'moment';

@Controller('auth')
export class AuthController {
  constructor(
    @Inject(UserModelToken) private readonly UserModel: Model<any>,
    private readonly authService: AuthService,
  ) {}

  @Post('register')
  async register(@Body() user: any) {
    const newUser = new this.UserModel(user);
    await newUser.save();
    return {
      status: 1,
      message: 'Success',
      data: {...UserSchema.getData(newUser), ...await this.authService.createToken(newUser)},
    };
  }

  @Post('login')
  async login(@Body() {username, password}: any) {
    const user = await this.UserModel.findOne({username}).exec();
    if (!user) throw new UnauthorizedException('Not found user');

    const passwordIsMatch = await bcrypt.compare(password, user.password);
    if (passwordIsMatch) {
      return {
        status: 1,
        message: 'Success',
        data: {...UserSchema.getData(user), ...await this.authService.createToken(user)},
      };
    }
    throw new UnauthorizedException('Password not match');
  }

  @Get('user-data')
  // @Roles('admin')
  @UseGuards(JwtAuthGuard)
  async userData(@Req() req) {
    return {
      status: 1,
      message: 'Success',
      data: {...UserSchema.getData(req.user), access_token: req.headers.authorization.replace('Bearer ', '')},
    };
  }

  @Put('users')
  @UseGuards(JwtAuthGuard)
  async editUser(@Body() {address, full_name, birthday, gender, phone, password}, @Req() req) {
    const body = {address, full_name, gender, phone} as any;
    if (birthday) {
      body.birthday = moment(birthday, 'MM/DD/YYYY').valueOf();
    }
    if (password) {
      body.password = bcrypt.hashSync(password, Math.floor(Math.random() * 9 + 1));
    }
    const user = await this.UserModel.findByIdAndUpdate(req.user._id, body, {new: true}).exec();

    return {
      status: 1,
      message: 'Success',
      data: {
        ...UserSchema.getData(user),
        ...await this.authService.createToken(user),
      },
    };
  }
}
