import { Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Model } from 'mongoose';
import { UserSchema } from '../database/schema/user.schema';
import { UserModelToken } from '../resource/static.resource';

@Injectable()
export class AuthService {
  constructor(
    @Inject(UserModelToken) private readonly UserModel: Model<any>,
    private readonly jwtService: JwtService,
  ) {}

  async createToken(user): Promise<any> {
    // console.log(user);
    const newUser = {
      id: user._id,
    };
    return {access_token: this.jwtService.sign(newUser)};
  }

  async validateUser(user: any): Promise<any> {
    const result = await this.UserModel.findById(user.id);
    return result;
  }
}
