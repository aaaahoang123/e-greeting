// var BASE_URL = "http://localhost:3000/";
var BASE_URL = "https://e-greeting.herokuapp.com/";

var app = angular.module("eGreeting", ["ngRoute"]);
var jq = jQuery.noConflict();

var HEADER_STATE_KEY = "JHGJHyutuityvnbyu";
var CATEGORY_PRODUCT_KEY = 'LDSJGKHSKHDsetiu';
var HOME_STATE_KEY = 'SAFKJH2sdfkjg';
var PRODUCTS_KEY = 'AdwaaAwkoamw';
var PERSONALIZE_STATE_KEY = "aaaaaaaaa0";

toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "pages/home.html",
            controller: 'homeController'
        })
        .when("/login", {
            templateUrl: "pages/login.html",
            controller: "loginController"
        })
        .when("/user", {
            templateUrl: "pages/user.html",
            controller: "userController"
        })
        .when("/register", {
            templateUrl: "pages/register.html",
            controller: "registerController"
        })
        .when("/category/:url", {
            templateUrl: "pages/products.html",
            controller: "categoryController"
        })
        .when("/products", {
            templateUrl: "pages/products.html",
            controller: "productsController"
        })
        .when("/contact", {
            templateUrl: "pages/contact.html",
            controller: "contactController"
        })
        .when("/premiums", {
            templateUrl: "pages/premiums.html",
            controller: "premiumController"
        })
        .when("/personalize/:url", {
            templateUrl: "pages/personalize.html",
            controller: "personalizeController"
        });
});

app.factory('Store', function () {
    // hold a local copy of the state, setting its defaults
    var state = {
        data: {}
    };
    // expose basic getter and setter methods
    return {
        get: function (key, default_obj) {
            if (!state.data[key]) {
                state.data[key] = default_obj;
            }
            return state.data[key];
        },
        set: function (key, data) {
            if (!state.data[key]) {
                state.data[key] = {};
            }
            Object.assign(state.data[key], data);
        }
    };
});

app.controller("headerController", function ($scope, Store, $http, $location, $route) {
    var state_define = {
        is_login: false,
        user: {},
        navbar_items: []
    };
    $scope.state = Store.get(HEADER_STATE_KEY, state_define);
    var access_token = localStorage.getItem('access_token');
    if (access_token) {

        $http.get(BASE_URL + 'auth/user-data', {
            headers: {
                authorization: 'Bearer ' + access_token,
                'Content-Type': 'application/json'
            }
        })
            .then(function (res) {
                $scope.state.is_login = true;
                $scope.state.user = res.data.data;
            })
            .catch(function (res) {
                $scope.state.is_login = false;
                $scope.state.user = {};
            });

    } else {
        $scope.state.is_login = false;
        $scope.state.user = {};
    }

    $http.get(BASE_URL + 'api/categories')
        .then(function (res) {
            $scope.state.navbar_items = res.data.datas;
        })
        .catch(function (res) {
            console.log(res);
        });

    $scope.logout = function () {
        localStorage.removeItem('access_token');
        $scope.state.is_login = false;
        $scope.state.user = {};
        Store.set(HEADER_STATE_KEY, $scope.state);
    };

    $scope.search = function () {
        var products_state = {
            request_query: {
                page: 1,
                limit: 12,
                search: ''
            }
        };
        $scope.products_state = Store.get(PRODUCTS_KEY, products_state);
        $scope.products_state.request_query.search = $scope.query;
        Store.set(PRODUCTS_KEY, $scope.products_state);
        $location.path('/products');
        $route.reload();
    }
});

app.controller("registerController", function ($scope, $http, Store, $location) {
    var header_state = Store.get(HEADER_STATE_KEY);
    $scope.is_submitting = false;
    if (header_state.is_login) {
        $location.path('/');
    }
    jq(document).ready(function () {
        jq("#datepicker").datepicker({
            onSelect(e) {
                var d = Date.UTC(e.split('/')[2], e.split('/')[0], e.split('/')[1]);
                $scope.registerData.birthday = d;
            }
        });
    });
    $scope.registerData = {
        username: '',
        password: '',
        confirmPassword: '',
        email: '',
        address: '',
        fullName: '',
        phone: '',
        gender: '', // 0 là chưa xác định, 1 là nam, 2 là nữ
        birthday: ''
    };

    $scope.passwordConfirmation = function () {
        return $scope.registerData.password === $scope.registerData.confirmPassword;
    };

    $scope.doSubmit = function () {
        $scope.is_submitting = true;
        $http.post(BASE_URL + 'auth/register', $scope.registerData)
            .then(function (res) {
                console.log(res);
                localStorage.setItem("access_token", res.data.data.access_token);
                var header_state = {
                    is_login: true,
                    user: res.data.data
                };
                Store.set(HEADER_STATE_KEY, header_state);
                toastr.success('Register success!');
                $location.path('/');
            })
            .catch(function (res) {
                console.log(res);
                toastr.error('Register fail');
            })
            .finally(function () {
                $scope.is_submitting = false;
            });
    }
});

app.controller("loginController", function ($scope, $http, Store, $location) {
    var header_state = Store.get(HEADER_STATE_KEY);
    if (header_state.is_login) {
        $location.path('/');
    }
    $scope.loginData = {
        username: '',
        password: ''
    };

    $scope.login = function () {
        $http.post(BASE_URL + 'auth/login', $scope.loginData)
            .then(function (res) {
                console.log(res);
                localStorage.setItem("access_token", res.data.data.access_token);
                var header_state = {
                    is_login: true,
                    user: res.data.data
                };
                Store.set(HEADER_STATE_KEY, header_state);
                toastr.success('Login success!');
                $location.path('/');

            })
            .catch(function (data) {
                toastr.error('Login fail!');
                console.log(data);
            })
    }
});

app.controller("userController", function ($scope, Store, $http, $location) {
    var header_state = Store.get(HEADER_STATE_KEY);
    var access_token = localStorage.getItem('access_token');

    if (!header_state.is_login) {
        $location.path('/login');
    }

    $scope.user = header_state.user;
    $scope.birthdayFormat = new Date($scope.user.birthday).toLocaleDateString('en');
    if ($scope.user.gender === 1 || $scope.user.gender === 2 || $scope.user.gender === 3) {
        $scope.genderFormat = $scope.user.gender.toString();
    } else {
        $scope.genderFormat = "";
    }

    $scope.save = function () {
        $scope.saving = true;
        $scope.user.birthday = $scope.birthdayFormat;
        $scope.user.gender = $scope.genderFormat;

        $http.put(BASE_URL + 'auth/users', $scope.user, {
            headers: {
                authorization: 'Bearer ' + access_token,
                'Content-Type': 'application/json'
            }
        })
            .then(function (res) {
                $scope.user = res.data.data;
                localStorage.setItem("access_token", res.data.data.access_token);
                header_state.user = $scope.user;
                Store.set(HEADER_STATE_KEY, header_state);

                $scope.saving = false;
                toastr.success('Saved!');
            })
            .catch(function (res) {
                $scope.saving = false;
                toastr.error(res.data.message);
            })
    };
    jq('document').ready(function () {
        jq("#datepicker").datepicker();
    });
});

app.controller("categoryController", function ($scope, Store, $http, $routeParams) {
    var state_default = {
        data: [],
        request_query: {
            page: 1,
            limit: 12,
            search: '',
            category: $routeParams.url
        },
        meta: {}
    };
    Store.set(CATEGORY_PRODUCT_KEY, state_default);
    $scope.state = Store.get(CATEGORY_PRODUCT_KEY, state_default);

    $scope.refresh = function (event) {
        if (event && event.keyCode !== 13) return;
        $http.get(BASE_URL + 'api/products', {params: $scope.state.request_query})
            .then(function (res) {
                $scope.products = res.data.datas;
                $scope.state.meta = res.data.meta;
            })
            .catch(function (res) {
                console.log(res);
            })
    };
    $scope.refresh();

    $scope.resizeImg = function (img_url) {
        var res = '';
        var strSp = 'https://res.cloudinary.com/fpt-aptech/image/upload/';
        var transform = 'w_250,h_140,c_scale';
        return res = strSp + transform + '/' + img_url.split(strSp)[1];
    }
});

app.controller('homeController', function ($scope, Store, $http, $location) {
    var state_default = {
        holidays: [],
        greetings: [],
        anniversaries: []
    };
    $scope.state = Store.get(HOME_STATE_KEY, state_default);

    if ($location.search().message) {
        toastr.info($location.search().message);
    }
    $http.get(BASE_URL + 'api/products', {params: {page: '1', limit: '4', category: 'holidays'}})
        .then(function (res) {
            $scope.state.holidays = res.data.datas;
        })
        .catch(function (res) {
            console.log(res);
        });

    $http.get(BASE_URL + 'api/products', {params: {page: '1', limit: '4', category: 'greeting-card'}})
        .then(function (res) {
            $scope.state.greetings = res.data.datas;
        })
        .catch(function (res) {
            console.log(res);
        });

    $http.get(BASE_URL + 'api/products', {params: {page: '1', limit: '4', category: 'anniversary'}})
        .then(function (res) {
            $scope.state.anniversaries = res.data.datas;
        })
        .catch(function (res) {
            console.log(res);
        });

    $scope.resizeImg = function (img_url) {
        var res = '';
        var strSp = 'https://res.cloudinary.com/fpt-aptech/image/upload/';
        var transform = 'w_250,h_140,c_scale';
        return res = strSp + transform + '/' + img_url.split(strSp)[1];
    }
});

app.controller("contactController", function ($scope, $http, Store, $location) {
    var state = Store.get(HEADER_STATE_KEY);
    var access_token = state.user.access_token;
    $scope.model = {
        message: ""
    };

    $scope.submit = function () {
      if (!state.is_login) {
        $location.path('/login');
        toastr.warning('You need to login to use this feature.');
        return;
      }
        $http.post(BASE_URL + 'api/reports', $scope.model, {
            headers: {
                authorization: 'Bearer ' + access_token,
                'Content-Type': 'application/json'
            }
        })
            .then(function (res) {
                toastr.success('Report success!');
            })
            .catch(function (res) {
                toastr.error('Report fail!');
            })
    }
});

app.controller("premiumController", function ($scope, $http) {
    var access_token = localStorage.getItem('access_token');
    $scope.is_buy = false;
    $http.get(BASE_URL + 'api/subscribe-packs')
        .then(function (res) {
            console.log(res);
            $scope.premiums = res.data.datas;
        })
        .catch()

    $scope.buy = function (id) {
        $scope.is_buy = true;
        $http.post(BASE_URL + 'api/transactions', {package: id}, {
            headers: {
                authorization: 'Bearer ' + access_token,
                'Content-Type': 'application/json'
            }
        })
            .then(function (res) {
                window.open(res.data.data, '_blank');
            })
            .catch()
    }
});

app.controller("productsController", function ($scope, $http, Store) {
    var state_default = {
        data: [],
        request_query: {
            page: 1,
            limit: 12,
            search: ''
        },
        meta: {}
    };
    $scope.state = Store.get(PRODUCTS_KEY, state_default);

    $scope.refresh = function (event) {
        if (event && event.keyCode !== 13) return;
        $http.get(BASE_URL + 'api/products', {params: $scope.state.request_query})
            .then(function (res) {
                $scope.products = res.data.datas;
                $scope.state.meta = res.data.meta;
            })
            .catch(function (res) {
                console.log(res);
            })
    };
    $scope.refresh();

    $scope.resizeImg = function (img_url) {
        var res = '';
        var strSp = 'https://res.cloudinary.com/fpt-aptech/image/upload/';
        var transform = 'w_250,h_140,c_scale';
        return res = strSp + transform + '/' + img_url.split(strSp)[1];
    }
});
app.controller("personalizeController", function ($scope, $http, Store, $window, $routeParams, $location) {
    var state_default = {
        data: {
            full_name: '',
            send_time: null,
            content: '',
            to_emails: ''
        },
        message: '',
        card_template: '',
        now_type: true,
        temp_send_time: null
    };

    $scope.state = Store.get(PERSONALIZE_STATE_KEY, state_default);
    $http.get(BASE_URL + 'pages/ecard.html')
        .then(function (res) {
            $scope.state.card_template = res.data;
        })
        .catch(console.log);
    if ($scope.state.url !== $routeParams.url) {
        $http.get(BASE_URL + 'api/products/' + $routeParams.url)
            .then(function (res) {
                Store.set(PERSONALIZE_STATE_KEY, {data: res.data.data});
                console.log($scope.state.data.image);
            })
            .catch(console.log);
    }
    console.log($scope.state);
    $scope.isValidSendTime = function() {
        return moment($scope.state.temp_send_time) > moment();
    };
    $scope.doSubmit = function () {
        var header = Store.get(HEADER_STATE_KEY);
        if (!header.is_login) {
            toastr.warning('You need to login to use this feature.');
            $location.path('/login');
            return;
        }
        console.log(moment(jq('#send_time').val()), moment(), moment(jq('#send_time').val()) < moment());
        ['full_name', 'to_emails'].forEach(function (field) {
            $scope.send_form[field].$setDirty();
        });
        if ($scope.send_form.$invalid && $scope.isValidSendTime) return;
        var dataToSend = {
            to_emails: $scope.state.data.to_emails.split(','),
            send_time: (!$scope.state.now_type && jq('#send_time').val()) ? moment(jq('#send_time').val()).format('YYYY-MM-DD HH:mm') : null,
            content: $scope.state.card_template
                .replace(new RegExp('{{video}}', 'g'), $scope.state.data.video)
                .replace(new RegExp('{{name}}', 'g'), $scope.state.data.full_name)
                .replace(new RegExp('{{image}}', 'g'), $scope.state.data.image)
                .replace(new RegExp('{{message}}', 'g'), $scope.state.message)
        };
        console.log($scope.state.message);
        console.log(dataToSend);
        $http.post(BASE_URL + 'api/schedules', dataToSend, {
            headers: {
                authorization: "Bearer " + localStorage.getItem('access_token')
            }
        })
            .then(function () {
                toastr.success('Send mail success!');
                $location.path('/');
            })
            .catch(function (errs) {
                // toastr.warning(errs.data.errors.message);
                console.log(errs);
                if (errs.data.message === "You must subscribe our service before using this feature") {
                    toastr.warning('You must subscribe our service before using this feature');
                    $location.path('/premiums');
                }
            });
    }
});